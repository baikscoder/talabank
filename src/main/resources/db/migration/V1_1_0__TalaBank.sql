-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: talabank
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbaccounts`
--

DROP TABLE IF EXISTS `tbaccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbaccounts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `accountno` varchar(255) DEFAULT NULL,
  `actualbal` double NOT NULL,
  `availablebal` double NOT NULL,
  `openingbal` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5vvjn5hqqjt8s2abnelk4bvys` (`accountno`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbaccounts`
--

LOCK TABLES `tbaccounts` WRITE;
/*!40000 ALTER TABLE `tbaccounts` DISABLE KEYS */;
INSERT INTO `tbaccounts` VALUES (1,'0001002547898',0,0,0);
/*!40000 ALTER TABLE `tbaccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbglcodes`
--

DROP TABLE IF EXISTS `tbglcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbglcodes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `balance` double DEFAULT NULL,
  `creditbalance` double DEFAULT NULL,
  `debitbalance` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbglcodes`
--

LOCK TABLES `tbglcodes` WRITE;
/*!40000 ALTER TABLE `tbglcodes` DISABLE KEYS */;
INSERT INTO `tbglcodes` VALUES (1,66000,66000,0,'Cash Withdraw','CWD'),(2,-70000,0,70000,'Cash Deposit','CDP');
/*!40000 ALTER TABLE `tbglcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtransactions`
--

DROP TABLE IF EXISTS `tbtransactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbtransactions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `narration` varchar(255) DEFAULT NULL,
  `accountno` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `dr_cr` varchar(255) DEFAULT NULL,
  `processingcode` varchar(255) DEFAULT NULL,
  `referenceno` varchar(255) DEFAULT NULL,
  `serialno` int NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtransactions`
--

LOCK TABLES `tbtransactions` WRITE;
/*!40000 ALTER TABLE `tbtransactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbtransactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'talabank'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-19 22:42:47
