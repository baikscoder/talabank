package com.talabank.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.talabank.controllers.Response;
import com.talabank.models.Tbaccounts;
import com.talabank.models.Tbglcodes;
import com.talabank.models.Tbtransactions;
import com.talabank.pojos.ISOFields;
import com.talabank.repositories.TbaccountRepository;
import com.talabank.repositories.TbglcodesRepository;
import com.talabank.repositories.TbtransactionsRepository;

@Service
public class TransactionsProcessor {
	@Autowired
	TbaccountRepository tbaccountRepository;
	@Autowired
	TbglcodesRepository tbglcodesRepository;
	@Autowired
	TbtransactionsRepository tbtransactionsRepository;

	public Response TransactionService(ISOFields isoFields) {
		Response response = new Response();
		switch (isoFields.getField3()) {
		case "310000":
			return BalanceInquiry(isoFields);
		case "210000":
			return CashDeposit(isoFields);
		case "010000":
			return CashWithdrwal(isoFields);
		}
		response.setReturnValue("Invalid Request");
		response.setStatusCode(HttpStatus.BAD_REQUEST);
		return response;
	}

	public Response BalanceInquiry(ISOFields isoFields) {
		Response response = new Response();

		try {

			List<Tbaccounts> account = tbaccountRepository.findByAccountno(isoFields.getField102());
			if (account.size() > 0) {
				response.setReturnValue(String.valueOf(account.get(0).getAvailablebal()));
				response.setStatusCode(HttpStatus.OK);
			} else {
				response.setReturnValue("Account does not exist");
				response.setStatusCode(HttpStatus.BAD_REQUEST);
			}
			CreateTransaction(isoFields, false, 1);
		} catch (Exception ex) {
			response.setReturnValue("An error occured!");
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public Response CashWithdrwal(ISOFields isoFields) {
		Response response = new Response();
		try {
			List<Tbaccounts> account = tbaccountRepository.findByAccountno(isoFields.getField102());
			List<Tbtransactions> transactions = tbtransactionsRepository.findTransaction(isoFields.getField102(),
					isoFields.getField3());
			Double transactionssum = tbtransactionsRepository.findTransactionSum(isoFields.getField102(),
					isoFields.getField3());
			if (account.size() <= 0) {
				response.setReturnValue("Account does not exist");
				response.setStatusCode(HttpStatus.BAD_REQUEST);
			} else if (Double.valueOf(isoFields.getField4()) <= 0) {
				response.setReturnValue("Invalid transaction amount: " + isoFields.getField4());
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else if (Double.valueOf(isoFields.getField4()) > account.get(0).getActualbal()) {
				response.setReturnValue("Insufficient funds");
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else if (transactionssum + Double.valueOf(isoFields.getField4()) >= 50000) {
				response.setReturnValue("Exceeded Maximum Withdrawal amount limit in a Day");
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else if (transactions.size() >= 3) {
				response.setReturnValue("Exceeded Maximum Withdrawals frequency in a Day");
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else {
				List<Tbglcodes> glcodes = tbglcodesRepository.findByTag("CWD");
				if (glcodes.size() <= 0) {
					response.setReturnValue("GL Code not configured");
					response.setStatusCode(HttpStatus.BAD_REQUEST);
				} else if (Double.valueOf(isoFields.getField4()) > 20000) {
					response.setReturnValue("Exceeded Maximum Withdrawal Per Transaction");
					response.setStatusCode(HttpStatus.FORBIDDEN);
				} else {
					// 1. DR Customer
					tbaccountRepository.UpdateAccountDR(isoFields.getField4(), isoFields.getField102());
					CreateTransaction(isoFields, true, 1);
					// 2. CR CWD GL
					tbglcodesRepository.UpdateGlCR(isoFields.getField4(), "CWD");
					isoFields.setField102("CWD");
					CreateTransaction(isoFields, false, 2);
					response.setReturnValue("Successful");
					response.setStatusCode(HttpStatus.OK);
				}
			}
		} catch (Exception ex) {
			response.setReturnValue("An error occured!");
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public Response CashDeposit(ISOFields isoFields) {
		Response response = new Response();
		try {
			List<Tbaccounts> account = tbaccountRepository.findByAccountno(isoFields.getField102());
			List<Tbtransactions> transactions = tbtransactionsRepository.findTransaction(isoFields.getField102(),
					isoFields.getField3());
			Double transactionssum = tbtransactionsRepository.findTransactionSum(isoFields.getField102(),
					isoFields.getField3());
			if (account.size() <= 0) {
				response.setReturnValue("Account does not exist");
				response.setStatusCode(HttpStatus.BAD_REQUEST);
			} else if (Double.valueOf(isoFields.getField4()) <= 0) {
				response.setReturnValue("Invalid transaction amount: " + isoFields.getField4());
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else if (transactionssum + Double.valueOf(isoFields.getField4()) >= 150000) {
				response.setReturnValue("Exceeded Maximum Deposit amount limit in a Day");
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else if (transactions.size() >= 4) {
				response.setReturnValue("Exceeded Maximum Deposits frequency in a Day");
				response.setStatusCode(HttpStatus.FORBIDDEN);
			} else {
				List<Tbglcodes> glcodes = tbglcodesRepository.findByTag("CDP");
				if (glcodes.size() <= 0) {
					response.setReturnValue("GL Code not configured");
					response.setStatusCode(HttpStatus.BAD_REQUEST);
				} else if (Double.valueOf(isoFields.getField4()) > 40000) {
					response.setReturnValue("Exceeded Maximum Deposit Per Transaction");
					response.setStatusCode(HttpStatus.FORBIDDEN);
				} else {
					// 1. DR Customer
					tbaccountRepository.UpdateAccountCR(isoFields.getField4(), isoFields.getField102());
					CreateTransaction(isoFields, true, 1);
					// 2. CR CWD GL
					tbglcodesRepository.UpdateGlDR(isoFields.getField4(), "CDP");
					isoFields.setField102("CDP");
					CreateTransaction(isoFields, false, 2);
					response.setReturnValue("Successful");
					response.setStatusCode(HttpStatus.OK);
				}
			}
		} catch (Exception ex) {
			response.setReturnValue("An error occured!");
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public void CreateTransaction(ISOFields isoFields, boolean dr, int serialno) {
		Tbtransactions tbtransactions = new Tbtransactions();
		tbtransactions.setAccountno(isoFields.getField102());
		tbtransactions.setAmount(Double.valueOf(isoFields.getField4()));
		tbtransactions.setNarration(isoFields.getField127());
		tbtransactions.setProcessingcode(isoFields.getField3());
		tbtransactions.setReferenceno(isoFields.getField37());
		tbtransactions.setSerialno(serialno);
		tbtransactions.setCurrency("USD");
		if (dr) {
			tbtransactions.setDr_cr("DR");
		} else {
			tbtransactions.setDr_cr("CR");
		}
		tbtransactionsRepository.save(tbtransactions);
	}

	public String generateRef(int count) {
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}

		return builder.toString();
	}

}
