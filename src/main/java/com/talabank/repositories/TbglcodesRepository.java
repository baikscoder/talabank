package com.talabank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.talabank.models.Tbglcodes;

@Repository
public interface TbglcodesRepository extends JpaRepository<Tbglcodes, Long> {
	List<Tbglcodes> findByTag(String tag);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "update tbglcodes set balance = balance- ?1 , debitbalance = debitbalance+ ?1 where tag= ?2")
	int UpdateGlDR(String amount, String tag);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "update tbglcodes set balance = balance+ ?1 ,creditbalance = creditbalance+ ?1 where tag= ?2")
	int UpdateGlCR(String amount, String tag);
}
