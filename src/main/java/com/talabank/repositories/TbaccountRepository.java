package com.talabank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.talabank.models.Tbaccounts;

import java.lang.String;
import java.util.List;

@Repository
public interface TbaccountRepository extends JpaRepository<Tbaccounts, Long> {
	List<Tbaccounts> findByAccountno(String accountno);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "update tbaccounts set actualbal = actualbal- ?1 , availablebal = availablebal- ?1 where accountno= ?2")
	int UpdateAccountDR(String amount, String accountno);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "update tbaccounts set actualbal = actualbal+ ?1 , availablebal = availablebal+ ?1 where accountno= ?2")
	int UpdateAccountCR(String amount, String accountno);
}
