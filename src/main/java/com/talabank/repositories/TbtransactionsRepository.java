package com.talabank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.talabank.models.Tbtransactions;

@Repository
public interface TbtransactionsRepository extends JpaRepository<Tbtransactions, Long> {
	@Query(nativeQuery = true, value = "select *from tbtransactions where CAST(created_at AS DATE)=CAST(now() AS DATE) and dr_cr='DR' and accountno=:accountno and processingcode=:processingcode")
	public List<Tbtransactions> findTransaction(@Param("accountno") String accountno,
			@Param("processingcode") String processingcode);

	@Query(nativeQuery = true, value = "select IFNULL(sum(amount),0) from tbtransactions where CAST(created_at AS DATE)=CAST(now() AS DATE) and dr_cr='DR' and accountno=:accountno and processingcode=:processingcode")
	public Double findTransactionSum(@Param("accountno") String accountno,
			@Param("processingcode") String processingcode);

}
