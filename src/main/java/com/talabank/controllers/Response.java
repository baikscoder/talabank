package com.talabank.controllers;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
	private HttpStatus StatusCode;
	private String ReturnValue;
}
