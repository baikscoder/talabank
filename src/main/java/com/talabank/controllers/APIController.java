package com.talabank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.talabank.pojos.ISOFields;
import com.talabank.pojos.Inquiry;
import com.talabank.pojos.Transfer;
import com.talabank.services.TransactionsProcessor;

@RestController
public class APIController {
	ISOFields isoFields = new ISOFields();

	@Autowired
	TransactionsProcessor transactionsProcessor;

	@PostMapping(value = "/Balance", consumes = { "application/json" })
	public ResponseEntity<?> Balance(@RequestBody Inquiry inquiry, @RequestHeader HttpHeaders headers) {
		Response response = new Response();
		isoFields.setField3("310000");
		isoFields.setField102(inquiry.getAccountno());
		isoFields.setField4("0");
		isoFields.setField37(transactionsProcessor.generateRef(12));
		isoFields.setField127("Balance Inquiry");
		response = transactionsProcessor.BalanceInquiry(isoFields);
		return new ResponseEntity<>(response.getReturnValue(), response.getStatusCode());
	}

	@PostMapping(value = "/Withdrawal", consumes = { "application/json" })
	public ResponseEntity<?> Withdrawal(@RequestBody Transfer transfer, @RequestHeader HttpHeaders headers) {
		Response response = new Response();
		isoFields.setField3("010000");
		isoFields.setField102(transfer.getAccountno());
		isoFields.setField4(transfer.getAmount());
		isoFields.setField37(transactionsProcessor.generateRef(12));
		isoFields.setField127("Cash Withdrawal");
		response = transactionsProcessor.CashWithdrwal(isoFields);
		return new ResponseEntity<>(response.getReturnValue(), response.getStatusCode());
	}

	@PostMapping(value = "/Deposit", consumes = { "application/json" })
	public ResponseEntity<?> Deposit(@RequestBody Transfer transfer, @RequestHeader HttpHeaders headers) {
		Response response = new Response();
		isoFields.setField3("210000");
		isoFields.setField102(transfer.getAccountno());
		isoFields.setField4(transfer.getAmount());
		isoFields.setField37(transactionsProcessor.generateRef(12));
		isoFields.setField127("Cash Deposit");
		response = transactionsProcessor.CashDeposit(isoFields);
		return new ResponseEntity<>(response.getReturnValue(), response.getStatusCode());
	}
}
