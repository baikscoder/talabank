package com.talabank.pojos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL) // ignore all null fields
@Getter
@Setter
public class ISOFields {

	public String field0;
	public String field1;
	public String field2;
	public String field3;
	public String field4;
	public String field5;
	public String field6;
	public String field7;
	public String field8;
	public String field9;
	public String field10;
	public String field11;
	public String field12;
	public String field13;
	public String field14;
	public String field15;
	public String field16;
	public String field17;
	public String field18;
	public String field19;
	public String field20;
	public String field21;
	public String field22;
	public String field23;
	public String field24;
	public String field25;
	public String field26;
	public String field27;
	public String field28;
	public String field29;
	public String field30;
	public String field31;
	public String field32;
	public String field33;
	public String field34;
	public String field35;
	public String field36;
	public String field37;
	public String field38;
	public String field39;
	public String field40;
	public String field41;
	public String field42;
	public String field43;
	public String field44;
	public String field45;
	public String field46;
	public String field47;
	public String field48;
	public String field49;
	public String field50;
	public String field51;
	public String field52;
	public String field53;
	public String field54;
	public String field55;
	public String field56;
	public String field57;
	public String field58;
	public String field59;
	public String field60;
	public String field61;
	public String field62;
	public String field63;
	public String field64;
	public String field65;
	public String field66;
	public String field67;
	public String field68;
	public String field69;
	public String field70;
	public String field71;
	public String field72;
	public String field73;
	public String field74;
	public String field75;
	public String field76;
	public String field77;
	public String field78;
	public String field79;
	public String field80;
	public String field81;
	public String field82;
	public String field83;
	public String field84;
	public String field85;
	public String field86;
	public String field87;
	public String field88;
	public String field89;
	public String field90;
	public String field91;
	public String field92;
	public String field93;
	public String field94;
	public String field95;
	public String field96;
	public String field97;
	public String field98;
	public String field99;
	public String field100;
	public String field101;
	public String field102;
	public String field103;
	public String field104;
	public String field105;
	public String field106;
	public String field107;
	public String field108;
	public String field109;
	public String field110;
	public String field111;
	public String field112;
	public String field113;
	public String field114;
	public String field115;
	public String field116;
	public String field117;
	public String field118;
	public String field119;
	public String field120;
	public String field121;
	public String field122;
	public String field123;
	public String field124;
	public String field125;
	public String field126;
	public String field127;
	public String field128;
}
