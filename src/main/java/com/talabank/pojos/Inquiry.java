package com.talabank.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Inquiry {
	private String accountno;
}
