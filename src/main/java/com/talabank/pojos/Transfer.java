package com.talabank.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transfer {
	private String accountno;
	private String amount;
}
