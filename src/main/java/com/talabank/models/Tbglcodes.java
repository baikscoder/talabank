package com.talabank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbglcodes")
@Getter
@Setter
public class Tbglcodes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String description;
	@Column(nullable = false)
	private String tag;
	@Column(precision = 10, scale = 2)
	private Double balance;
	@Column(precision = 10, scale = 2)
	private Double creditbalance;
	@Column(precision = 10, scale = 2)
	private Double debitbalance;
}