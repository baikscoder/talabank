package com.talabank.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbtransactions")
@Getter
@Setter
public class Tbtransactions {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String accountno;
	private String referenceno;
	private int serialno;
	private String currency;
	private String dr_cr;
	private String processingcode;
	@Column(precision = 10, scale = 2)
	private Double amount;
	@CreationTimestamp
	@Column(name = "created_at")
	private Timestamp createdat;
	private String Narration;

}