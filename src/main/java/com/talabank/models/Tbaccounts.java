package com.talabank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbaccounts")
@Getter
@Setter
public class Tbaccounts {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true)
	private String accountno;
	@Column(precision = 10, scale = 2, nullable = false)
	private Double openingbal = 0.0;
	@Column(precision = 10, scale = 2, nullable = false)
	private Double actualbal = 0.0;
	@Column(precision = 10, scale = 2, nullable = false)
	private Double availablebal = 0.0;

}
