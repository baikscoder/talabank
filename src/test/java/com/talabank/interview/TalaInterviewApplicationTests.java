package com.talabank.interview;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.google.gson.Gson;
import com.talabank.pojos.Inquiry;
import com.talabank.pojos.Transfer;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class TalaInterviewApplicationTests {
	@LocalServerPort
	private int port;
	@Autowired
	TestRestTemplate testRestTemplate;
	Gson gson = new Gson();

	@Test
	void contextLoads() {
	}

	@Test
	public void Balance() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		Inquiry inquiry = new Inquiry();
		inquiry.setAccountno("0001002547898");
		HttpEntity<Object> entity = new HttpEntity<Object>(gson.toJson(inquiry), httpHeaders);
		String response = testRestTemplate
				.postForEntity("http://localhost:" + port + "TalaBank/Balance", entity, String.class).getBody();
		System.out.println("Balance:: " + response);
		assertThat(response).isNotEmpty();
	}

	@Test
	public void Withdrawal() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		Transfer transfer = new Transfer();
		transfer.setAccountno("0001002547898");
		transfer.setAmount("4000");
		HttpEntity<Object> entity = new HttpEntity<Object>(gson.toJson(transfer), httpHeaders);
		String response = testRestTemplate
				.postForEntity("http://localhost:" + port + "TalaBank/Withdrawal", entity, String.class).getBody();
		System.out.println("Withdrawal:: " + response);
		assertThat(response).isNotEmpty();
	}

	@Test
	public void Deposit() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		Transfer transfer = new Transfer();
		transfer.setAccountno("0001002547898");
		transfer.setAmount("4000");
		HttpEntity<Object> entity = new HttpEntity<Object>(gson.toJson(transfer), httpHeaders);
		String response = testRestTemplate
				.postForEntity("http://localhost:" + port + "TalaBank/Deposit", entity, String.class).getBody();
		System.out.println("Deposit:: " + response);
		assertThat(response).isNotEmpty();
	}

}
