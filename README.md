## DATABASE SET UP

```sh
AUTO
```
* Change the database configurations on "/src/main/resources/application.properties" to match your environment.
```sh
###Database config 
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/talabank?createDatabaseIfNotExist=true
spring.datasource.username=root
spring.datasource.password=baiks@123
```
* The database should be auto-created according to the setting "createDatabaseIfNotExist=true".
* The necessary tables will be auto-insert by Flyway.

```sh
MANUAL
```
* Create a database with the schema name as "talabank".
* Import the sql script "/src/main/resources/db/migration/V1_1_0__TalaBank.sql".



## APPLICATION SET UP

* Open the application using any of your favourite IDE i.e Netbeans, Eclipse, Spring-tool suite, Intelli-J.
* Run the application.
* Browse the url "http://localhost:2021/TalaBank/swagger-ui.html#/" to access swagger-ui.
* Run your tests. The default account set on DB is "0001002547898".

## UNIT TESTING AND CODE COVERAGE
* Open commadline and navigate to the path where you cloned the application.
* Run the command "mvn clean install".
* Open the folder where you clone the application i.e projectfolder/target/site.
* The folder contains the code coverage files. You can open index.html and navigate where you want.




